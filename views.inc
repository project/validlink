<?php
/**
 * Implementation of hook_views_tables()
 */
function validlink_views_tables() {
	$tables['validlink'] = array(
		'name' => 'validlink',
		'join' => array(
			'left' => array(
				'table' => 'node',
				'field' => 'nid'
			),
			'right' => array(
				'field' => 'nid'
			),
		),
		'fields' => array(
			'tag'=>array(
				'name' => t('Validlink tags'),
				'sortable' => true,
				'help' => t('Show tags found by validlink with link address.'),
			),
			'link'=>array(
				'name' => t('Validlink link'),
				'sortable' => true,
				'help' => t('Show url found by validlink.'),
			),
			'error'=>array(
				'name' => t('Validlink error'),
				'sortable' => true,
				'help' => t('Validlink error message.'),
			),
			'dt_lastcheck' => array(
				'name' => t('Validlink last check'),
				'sortable' => true,
				'handler' => views_handler_field_dates(),
				'option' => 'string',
				'help' => t('Last datetime checked by validlink.'),
			),
		),
		'filters' => array(
			'nid' => array(
				'name' => t('Validlink: has links'),
				'operator' => array('=' => t('Exists')),
				'list' => 'views_handler_operator_yesno',
				'list-type' => 'select',
				'handler' => 'validlink_views_handler_errorlinks_exist',
				'help' => t('Filter whether the node has links or not.'),
			),
		),

	);
	return $tables;
}

function validlink_views_handler_errorlinks_exist($op, $filter, $filterdata, &$query) {
	switch ($op) {
		case 'handler':
			$query->ensure_table('validlink');
			if ($filter['value']) {
				$query->set_distinct();
				$table_data = _views_get_tables();
				$joins = array('type' => 'inner');
				$joins = array_merge($joins, $table_data['validlink']['join']);
				$query->joins['validlink'][1] = $joins;
			}
			else {
				$query->add_where('ISNULL(validlink.lid)');
			}
	}
}

?>