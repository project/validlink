
Valid link

The module performs validation of links in node body after node submit and shows if any links are broken.
Also the module checks all links with cron job and sends notification to node authors if any link become not available.

Uses notification_emails module 
 
To install, place the entire module folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable the Valid link module.

To set up access rights go to 
Administer -> User management -> Access Control

To change module settings go to  
Administer -> Site Configuration -> Valid Links

To check list of broken links on the site go to 
Administer -> Logs -> Invalid Links